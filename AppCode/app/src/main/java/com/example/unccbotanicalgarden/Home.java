package com.example.unccbotanicalgarden;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class Home extends AppCompatActivity {

    Button btnSearchDirectory,btnNavigator, btnGallery, btnEvent;

    Firebase mFirebaseRef;
    ArrayList<Plant> plantList;
    ArrayList<String> imageList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("  UNCC Botanical Garnden");
        toolbar.setLogo(R.mipmap.ic_launcher);
        toolbar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorGreen)));
       // bar.setDisplayShowTitleEnabled(false);
        setSupportActionBar(toolbar);
        ImageView mainImage = (ImageView) findViewById(R.id.main_image);

        Firebase.setAndroidContext(Home.this);
        mFirebaseRef = new Firebase("https://botanicagarden.firebaseio.com/plants");

        plantList = new ArrayList<>();
        imageList =new ArrayList<>();

        Picasso.with(getApplicationContext())
                .load("http://cdn.onlyinyourstate.com/wp-content/uploads/2015/11/UNCC-cone-entry1.jpg")
                //.fit()
                .into(mainImage);

        btnSearchDirectory = (Button)findViewById(R.id.btn_search);
        //btnTrails = (Button)findViewById(R.id.btn_trails);
        btnNavigator = (Button)findViewById(R.id.btn_navigator);
        btnGallery = (Button)findViewById(R.id.btn_gallery);
        btnEvent = (Button)findViewById(R.id.btn_event);

        btnEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Home.this,Events.class);
                startActivity(intent);
            }
        });

        btnSearchDirectory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Home.this,Search.class);
                startActivity(intent);
            }
        });
        btnGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(Home.this, Gallery.class);
                startActivity(i);
            }
        });

        btnNavigator.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Home.this,Navigator.class);
                startActivity(i);
            }
        });


//        btnTrails.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i = new Intent(Home.this,Trails.class);
//                startActivity(i);
//            }
//        });


      }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_info) {

            Intent i = new Intent(Home.this,Info.class);
            startActivity(i);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
