package com.example.unccbotanicalgarden;

import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class FullscreenImage extends AppCompatActivity {

    ArrayList<String> imageList;
    int position;
    ImageView fullscreenimg, preBtn, nextBtn, cancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
         super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_fullscreen_image);
//        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
//        actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorGreen)));
//        actionBar.setTitle("");
//        actionBar.setDisplayShowHomeEnabled(true);
//        actionBar.setIcon(R.mipmap.ic_launcher);
//actionBar.setTitle();

        imageList = new ArrayList<>();
        position = 0;
        fullscreenimg = (ImageView)findViewById(R.id.fullscreenImage);
        nextBtn = (ImageView)findViewById(R.id.nextBtn);
        preBtn =  (ImageView) findViewById(R.id.preBtn);
        cancel =  (ImageView) findViewById(R.id.cancel);

        if(getIntent()!= null){
            imageList = getIntent().getStringArrayListExtra("IMAGESLINKS");
            position = getIntent().getIntExtra("INDEX",0);
            Log.d("imagelist"," size = "+imageList.size());
            Log.d("imagelist"," position = "+position);

            loadImage(imageList, position);
        }

        preBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(imageList!= null)
                {
                    if(position==0){position = imageList.size()-1;}
                    else{position = position-1;}
                    loadImage(imageList,position);
                }
            }
        });

        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(imageList!= null)
                {
                    if(position==imageList.size()-1){position = 0;}
                    else{position = position+1;}
                    loadImage(imageList,position);

                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }

    public void loadImage(ArrayList<String> images, int index)
    {
        Picasso.with(FullscreenImage.this)
                .load(images.get(index))
                .fit()
                .into(fullscreenimg);

    }
}







