package com.example.unccbotanicalgarden;

import android.content.Intent;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.squareup.picasso.Picasso;

public class Navigator extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    LatLng currentLatLng;
    Plant currentPlant;
    POI currentPOI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigator);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        currentPlant = null;
        currentPOI = null;
        currentLatLng=null;

    }


    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);


        if (getIntent().getSerializableExtra("CURRENTPLANT")!= null) {
            currentPlant = (Plant)getIntent().getSerializableExtra("CURRENTPLANT");
            Log.d("Current Plant Location ","=="+Double.parseDouble(currentPlant.getLatittute())+Double.parseDouble(currentPlant.getLongitude()));
            currentLatLng = new LatLng(Double.parseDouble(currentPlant.getLatittute()),Double.parseDouble(currentPlant.getLongitude()));
            mMap.addMarker(new MarkerOptions().position(currentLatLng).infoWindowAnchor(.5f, .5f).icon(BitmapDescriptorFactory.fromResource(R.drawable.markericon)));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 19));//
        }
        if(getIntent().getSerializableExtra("CURRENTPOI")!= null){
            currentPOI = (POI)getIntent().getSerializableExtra("CURRENTPOI");
            Log.d("Current POI Location","=="+Double.parseDouble(currentPOI.getLongitude())+Double.parseDouble(currentPOI.getLatittute()));
            currentLatLng = new LatLng(Double.parseDouble(currentPOI.getLongitude()),Double.parseDouble(currentPOI.getLatittute()));
            mMap.addMarker(new MarkerOptions().position(currentLatLng).infoWindowAnchor(.5f, .5f).icon(BitmapDescriptorFactory.fromResource(R.drawable.markericon)));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 19));//
        }
        else
        {
            currentLatLng = new LatLng(35.30809332, -80.72883406);
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 19));//
//            currentLatLng = null;
        }

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                marker.showInfoWindow();
                return true;
            }
        });

        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                View v = getLayoutInflater().inflate(R.layout.info_window, null);
                TextView text = (TextView) v.findViewById(R.id.poi_description);
                text.setText(currentPlant.getCommon_name());
                ImageView image = (ImageView) v.findViewById(R.id.poi_Image);
                Picasso.with(getApplicationContext())
                        .load(currentPlant.getImg())
                        .into(image);

                return v;
            }
        });


        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                // if (marker.getTitle().toString().equalsIgnoreCase("Point B")) {
                //Intent i = new Intent(Navigator.this, ShowImage.class);
                //  startActivity(i);
                // }
            }
        });
        //addCoordinates();
        drawLine();
    }

    private void addCoordinates() {




    }

    private void drawLine() {

        PolylineOptions gardenPath = new PolylineOptions();
        PolylineOptions gardenPath1 = new PolylineOptions();

      //  mMap.addMarker(new MarkerOptions().position(new LatLng(35.308509, -80.727885)).title("East Gate").infoWindowAnchor(.5f, .5f).icon(BitmapDescriptorFactory.fromResource(R.drawable.asiangate)));
       // mMap.addMarker(new MarkerOptions().position(new LatLng(35.308460, -80.729007)).title("Sun Mosiac").infoWindowAnchor(.5f, .5f).icon(BitmapDescriptorFactory.fromResource(R.drawable.sun)));
        //mMap.addMarker(new MarkerOptions().position(new LatLng(35.308529, -80.728505)).title("Bench").infoWindowAnchor(.5f, .5f).icon(BitmapDescriptorFactory.fromResource(R.drawable.bench)));
      //  mMap.addMarker(new MarkerOptions().position(new LatLng(35.308196, -80.728680)).title("Butterfly Pergola").infoWindowAnchor(.5f, .5f).icon(BitmapDescriptorFactory.fromResource(R.drawable.gazebo)));
       // mMap.addMarker(new MarkerOptions().position(new LatLng(35.308049, -80.729194)).title("Butterfly Pergola").infoWindowAnchor(.5f, .5f).icon(BitmapDescriptorFactory.fromResource(R.drawable.camelliawalk)).rotation(-90));
       // mMap.addMarker(new MarkerOptions().position(new LatLng(35.30828327, -80.728211)).title("Moon Gate").infoWindowAnchor(.5f, .5f).icon(BitmapDescriptorFactory.fromResource(R.drawable.moongate)));

        //35.308307, -80.728193

        gardenPath.add(new LatLng(35.308395, -80.727862));//East gate
        gardenPath.add(new LatLng(35.307702, -80.729260));//Marry Alex rd
        gardenPath.add(new LatLng(35.308623, -80.729284));//Marry Alex rd end corner
        gardenPath.add(new LatLng(35.308645, -80.729079));//Marry Alex rd curve

        mMap.setMyLocationEnabled(true);


//        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
//
//        // Creating a criteria object to retrieve provider
//        Criteria criteria = new Criteria();
//
//        // Getting the name of the best provider
//        String provider = locationManager.getBestProvider(criteria, true);
//
//        // Getting Current Location
//        Location location = locationManager.getLastKnownLocation(provider);
//
//        Log.d("myLocation: ",location.getLatitude()+" locationmanager");
//
//
//        Location myLocation  = mMap.getMyLocation();
//        Log.d("myLocation: ",myLocation.getLatitude()+" location");
//        float[] results = new float[1];
//        Location.distanceBetween(35.308395, -80.727862,35.308645, -80.729079,results);


//        for (int i=0; i<results.length;i++)
//        {
//            Log.d("Distance",(results[i]*0.00062137)+"m");
//        }

        gardenPath1.color(Color.RED);
        gardenPath1.width(6f);
        gardenPath.color(Color.RED);
        gardenPath.width(6f);
        // mMap.addPolyline(gardenPath);
        //  mMap.addPolyline(gardenPath1);

        drawMap();
        drawWintertrail();

    }

    private void drawMap() {

        PolylineOptions winterTrail = new PolylineOptions();
//Winter trail
        winterTrail.add(new LatLng(35.307751, -80.729112));//gate 3
        winterTrail.add(new LatLng(35.307859, -80.728942));
        winterTrail.add(new LatLng(35.307934, -80.728976));
        //winterTrail.add(new LatLng(35.307898, -80.728993));
//        winterTrail.add(new LatLng(35.307796, -80.729018));
        winterTrail.add(new LatLng(35.307993, -80.729098));
        winterTrail.add(new LatLng(35.308072, -80.729085));
        winterTrail.add(new LatLng(35.308108, -80.729074));
        winterTrail.add(new LatLng(35.308105, -80.729070));
        winterTrail.add(new LatLng(35.308178, -80.729052));
        winterTrail.add(new LatLng(35.308242, -80.729039));
        winterTrail.add(new LatLng(35.308291, -80.729053));
        winterTrail.add(new LatLng(35.308358, -80.729063));
        winterTrail.add(new LatLng(35.308437, -80.729081));
        winterTrail.add(new LatLng(35.308510, -80.729087));
        winterTrail.add(new LatLng(35.308581, -80.729101));
        winterTrail.add(new LatLng(35.30865935, -80.72901492));//end of commented part
        // winterTrail.add(new LatLng(35.308619, -80.729137));
        winterTrail.color(Color.WHITE);
        winterTrail.width(13f);
        mMap.addPolyline(winterTrail);

        //Arbor trail
        PolylineOptions arborTrail = new PolylineOptions();
        arborTrail.add(new LatLng(35.307934, -80.728976));
        arborTrail.add(new LatLng(35.308006, -80.728978));
        arborTrail.add(new LatLng(35.308079, -80.728983));
        arborTrail.add(new LatLng(35.308128, -80.728968));
        arborTrail.add(new LatLng(35.308175, -80.728947));
        arborTrail.add(new LatLng(35.308240, -80.728952));
        arborTrail.add(new LatLng(35.308296, -80.728943));
        arborTrail.add(new LatLng(35.308365, -80.728929));
        arborTrail.add(new LatLng(35.308423, -80.728919));
        arborTrail.add(new LatLng(35.308480, -80.728927));
        arborTrail.add(new LatLng(35.308587, -80.728967));
        arborTrail.add(new LatLng(35.30865935, -80.72901492));//end of commented part

        arborTrail.color(Color.WHITE);
        arborTrail.width(13f);
        mMap.addPolyline(arborTrail);


        //pond trail
        PolylineOptions pondTrail = new PolylineOptions();

        pondTrail.add(new LatLng(35.308307, -80.728193));
        pondTrail.add(new LatLng(35.30828323, -80.728216));
        pondTrail.add(new LatLng(35.30828327, -80.728231));

        pondTrail.add(new LatLng(35.30816201, -80.72855736));//corner point after moon gate
       // pondTrail.add(new LatLng(35.30821707, -80.72843941));
        pondTrail.add(new LatLng(35.30830905, -80.72858782));
        pondTrail.add(new LatLng(35.30831449, -80.72860755));
        pondTrail.add(new LatLng(35.30834102, -80.72866471));
        pondTrail.add(new LatLng(35.30828886, -80.72866167));
        pondTrail.add(new LatLng(35.30828886, -80.72866167));
        pondTrail.add(new LatLng( 35.308197, -80.728668) );
        pondTrail.add( new LatLng( 35.308202, -80.728710) );
        pondTrail.add(new LatLng( 35.308242, -80.728730 ));
        pondTrail.add(new LatLng( 35.308268, -80.728757 ));
        pondTrail.add(new LatLng(  35.308267, -80.728811));
        pondTrail.add(new LatLng( 35.308298, -80.728864 ));
        pondTrail.add(new LatLng(35.308324, -80.728940));
        //pondTrail.add(new LatLng(35.308322, -80.728982));

        pondTrail.color(Color.WHITE);
        pondTrail.width(13f);
        mMap.addPolyline(pondTrail);

        PolylineOptions asianGraden = new PolylineOptions();
        asianGraden.add(new LatLng(35.308529, -80.727916));
        asianGraden.add(new LatLng(35.308509, -80.727885));
        asianGraden.add(new LatLng(35.308466, -80.727900));
        asianGraden.add(new LatLng(35.308440, -80.727926));
        asianGraden.add(new LatLng(35.308440, -80.727926));
        asianGraden.add(new LatLng(35.308387, -80.727961));
        asianGraden.add(new LatLng(35.308386, -80.728010));

        asianGraden.add(new LatLng(35.308377, -80.728050));
        asianGraden.add(new LatLng(35.308342, -80.728074));
        asianGraden.add(new LatLng(35.308321, -80.728101));
        asianGraden.add(new LatLng(35.308303, -80.728127));
        asianGraden.add(new LatLng(35.308289, -80.728157));
        asianGraden.add(new LatLng(35.308307, -80.728193));
        asianGraden.add(new LatLng(35.308344, -80.728214));
        asianGraden.add(new LatLng(35.308373, -80.728202));
        asianGraden.add(new LatLng(35.308380, -80.728180));
        asianGraden.add(new LatLng(35.308380, -80.728180));
        asianGraden.add(new LatLng(35.308380, -80.728180));
        asianGraden.add(new LatLng(35.308390, -80.728149));
        asianGraden.add(new LatLng(35.308377, -80.728136));
        asianGraden.add(new LatLng(35.308382, -80.728096));
        asianGraden.add(new LatLng(35.308379, -80.728054));


        asianGraden.color(Color.WHITE);
        asianGraden.width(13f);
        mMap.addPolyline(asianGraden);

        PolylineOptions butterflyGarden = new PolylineOptions();

        butterflyGarden.add(new LatLng(35.308529, -80.727916));
        butterflyGarden.add(new LatLng(35.308548, -80.727965));
        butterflyGarden.add(new LatLng(35.308561, -80.728015));
        butterflyGarden.add(new LatLng(35.308553, -80.728074));
        butterflyGarden.add(new LatLng(35.308542, -80.728116));
        butterflyGarden.add(new LatLng(35.308534, -80.728164));
        butterflyGarden.add(new LatLng(35.308557, -80.728173));
        butterflyGarden.add(new LatLng(35.308582, -80.728195));
        butterflyGarden.add(new LatLng(35.308639, -80.728247));
        butterflyGarden.add(new LatLng(35.308664, -80.728267));
        butterflyGarden.add(new LatLng(35.308670, -80.728305));
        butterflyGarden.add(new LatLng(35.308697, -80.728377));
        butterflyGarden.add(new LatLng(35.308718, -80.728439));
        butterflyGarden.add(new LatLng(35.308755, -80.728458));

        butterflyGarden.color(Color.WHITE);
        butterflyGarden.width(13f);
        mMap.addPolyline(butterflyGarden);

        PolylineOptions centerTrail = new PolylineOptions();

        centerTrail.add(new LatLng(35.30868125, -80.72867186));
        centerTrail.add(new LatLng(35.308656, -80.728599));
        centerTrail.add(new LatLng(35.308630, -80.728566));
        centerTrail.add(new LatLng(35.308598, -80.728528));
        centerTrail.add(new LatLng( 35.308568, -80.728517 ));
        centerTrail.add(new LatLng( 35.308530, -80.728488 ));
        centerTrail.add(new LatLng( 35.308522, -80.728453));
        centerTrail.add(new LatLng(35.308541, -80.728405));
        centerTrail.add(new LatLng(35.308564, -80.728356));
        centerTrail.add(new LatLng(35.308568, -80.728313));
        centerTrail.add(new LatLng( 35.308556, -80.728272 ));
        centerTrail.add(new LatLng(35.308532, -80.728218  ));
        centerTrail.add(new LatLng(35.308534, -80.728160));

        centerTrail.color(Color.WHITE);
        centerTrail.width(13f);
        mMap.addPolyline(centerTrail);

//        PolylineOptions circlePath = new PolylineOptions();
//        circlePath.add(new LatLng( 35.308669, -80.728870 ));
//        circlePath.add(new LatLng(  35.308697, -80.728877 ));
//        circlePath.add( new LatLng( 35.308707, -80.728865  ));
//        circlePath.add( new LatLng( 35.308713, -80.728843  ));
//        circlePath.add(new LatLng( 35.308708, -80.728823  ));
//        circlePath.add(new LatLng(35.308688, -80.728817));
//        circlePath.color(Color.BLACK);
//        circlePath.width(10f);
//        mMap.addPolyline(circlePath);


        PolylineOptions gardenPath = new PolylineOptions();
        gardenPath.add(new LatLng(35.30865935, -80.72901492));//end of commented part
        gardenPath.add(new LatLng(35.30868196, -80.72890724));
        gardenPath.add(new LatLng(35.30869285, -80.72888375));
        gardenPath.add(new LatLng(35.30867195, -80.72869473));
        gardenPath.add(new LatLng(35.30868125, -80.72867186));
        gardenPath.add(new LatLng(35.30868824, -80.72864694));
        gardenPath.add(new LatLng(35.30866888, -80.72843324));
        gardenPath.add(new LatLng(35.3086608, -80.72829929));

        gardenPath.color(Color.WHITE);
        gardenPath.width(13f);
        mMap.addPolyline(gardenPath);



        PolylineOptions waterGarden = new PolylineOptions();

        waterGarden.add(new LatLng(35.308196, -80.728680));
        waterGarden.add(new LatLng( 35.308202, -80.728628));
        waterGarden.add(new LatLng(35.308190, -80.728621));
        waterGarden.add(new LatLng(35.308165, -80.728623));
        waterGarden.add(new LatLng(35.308114, -80.728652));
        waterGarden.add(new LatLng(35.308091, -80.728650));
        waterGarden.add(new LatLng(35.308067, -80.728688));
        waterGarden.add(new LatLng(35.308052, -80.728710));
        waterGarden.add(new LatLng( 35.308047, -80.728735));
        waterGarden.add(new LatLng(35.308055, -80.728756));
        waterGarden.add(new LatLng(35.308069, -80.728780));
        waterGarden.add(new LatLng(35.308090, -80.728793));
        waterGarden.add(new LatLng(35.308118, -80.728793));
        waterGarden.add(new LatLng(35.308136, -80.728783));
        waterGarden.add(new LatLng(35.308147, -80.728768));
        waterGarden.add(new LatLng(35.308162, -80.728750));
        waterGarden.add(new LatLng(35.308208, -80.728714));





//        waterGarden.add(new LatLng(35.308188, -80.728731));
//        waterGarden.add(new LatLng(35.308147, -80.728782));
//        waterGarden.add(new LatLng(35.308142, -80.728783));
//        waterGarden.add(new LatLng(35.308115, -80.728789));
//        waterGarden.add(new LatLng(35.308081, -80.728800));
//        waterGarden.add(new LatLng(35.308035, -80.728798));
//
//        gardenPath.add(new LatLng(35.30865935, -80.72901492));//end of commented part
//        gardenPath.add(new LatLng(35.30868196, -80.72890724));
//        gardenPath.add(new LatLng(35.30869285, -80.72888375));
//        gardenPath.add(new LatLng(35.30867195, -80.72869473));
//        gardenPath.add(new LatLng(35.30868125, -80.72867186));
//        gardenPath.add(new LatLng(35.30868824, -80.72864694));
//        gardenPath.add(new LatLng(35.30866888, -80.72843324));
//        gardenPath.add(new LatLng(35.3086608, -80.72829929));

        waterGarden.color(Color.WHITE);
        waterGarden.width(13f);
        mMap.addPolyline(waterGarden);


    }

    private void drawWintertrail() {
        PolygonOptions polyGon1 = new PolygonOptions()
                .add(
                        new LatLng(35.308669, -80.729275),  // Start 1
                        new LatLng(35.307689, -80.729276),// MaryAlex 2
                        new LatLng(35.307751, -80.729112),//gate 3
                        new LatLng(35.307796, -80.729018),
                        new LatLng(35.307993, -80.729098),
                        new LatLng(35.308072, -80.729085),
                        new LatLng(35.308108, -80.729074),
                        new LatLng(35.308105, -80.729070),
                        new LatLng(35.308178, -80.729052),
                        new LatLng(35.308242, -80.729039),
                        new LatLng(35.308291, -80.729053),
                        new LatLng(35.308358, -80.729063),
                        new LatLng(35.308437, -80.729081),
                        new LatLng(35.308510, -80.729087),
                        new LatLng(35.308581, -80.729101),
                        new LatLng(35.308619, -80.729137))
                        //.strokeColor(Color.BLUE)
                .strokeWidth(0)
                .fillColor(Color.parseColor("#cef9ce"));//A2FDBF

        PolygonOptions pond = new PolygonOptions()
                .add(
                        new LatLng(35.308247, -80.728795),
                        new LatLng(35.308224, -80.728799),
                        new LatLng(35.308199, -80.728797),
                        new LatLng(35.308175, -80.728788),
                        new LatLng(35.308167, -80.728775),
                        new LatLng(35.308162, -80.728753),
                        new LatLng(35.308172, -80.728737),
                        new LatLng(35.308196, -80.728726),
                        new LatLng(35.308220, -80.728725),
                        new LatLng(35.308244, -80.728735),
                        new LatLng(35.308253, -80.728760),
                        new LatLng(35.308255, -80.728781))
                .strokeWidth(0)
                .fillColor(Color.CYAN);


        PolygonOptions polyGon2 = new PolygonOptions()
                .add(new LatLng(35.308525, -80.728948), //Arbor trail and wintrail merg point
                        new LatLng(35.308480, -80.728927),
                        new LatLng(35.308423, -80.728919),
                        new LatLng(35.308365, -80.728929),
                        new LatLng(35.308296, -80.728943),
                        new LatLng(35.308240, -80.728952),
                        new LatLng(35.308175, -80.728947),
                        new LatLng(35.308128, -80.728968),
                        new LatLng(35.308079, -80.728983),
                        new LatLng(35.308006, -80.728978),
                        new LatLng(35.307934, -80.728976),
                        new LatLng(35.307905, -80.728987),
                        new LatLng(35.307861, -80.728973))
                .strokeWidth(0)
                .fillColor(Color.parseColor("#cff1f2"));

        PolygonOptions outline = new PolygonOptions()
                .add(
                        new LatLng(35.307692, -80.729289),
                        new LatLng(35.307757, -80.729130),
                        new LatLng(35.308308, -80.728067),

//                        new LatLng(35.307874, -80.729000),
                        new LatLng(35.308051, -80.728543),
                        new LatLng(35.308308, -80.728067),
                        new LatLng(35.308401, -80.727857),

                        //new LatLng(35.308361, -80.727902),
                        new LatLng(35.308506, -80.727848),
                        new LatLng(35.308575, -80.727887),
                        new LatLng(35.308659, -80.727972),
                        new LatLng(35.308677, -80.728017),
                        new LatLng(35.308655, -80.728102),
                        new LatLng(35.308814, -80.728168),
                        new LatLng(35.308819, -80.728326),
                        new LatLng(35.308850, -80.728468),
                        new LatLng(35.308900, -80.728632),
                        new LatLng(35.308879, -80.728760),
                        new LatLng(35.308867, -80.728848),
                        new LatLng(35.308782, -80.728897),
                        new LatLng(35.308699, -80.729030),
                        new LatLng(35.308634, -80.729152),
                        new LatLng(35.308634, -80.729243),
                        new LatLng(35.308592, -80.729285))
                        //.strokeColor(Color.BLUE)
                .strokeWidth(0)
                .fillColor(Color.parseColor("#90EE90"));//
        //Polygon myWinterPoly1 = mMap.addPolygon(polyGon1);
        Polygon outlinePoly = mMap.addPolygon(outline);
        Polygon pondPoly = mMap.addPolygon(pond);


        ;


    }


}
