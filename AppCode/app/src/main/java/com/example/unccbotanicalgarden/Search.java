package com.example.unccbotanicalgarden;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.Toast;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.android.gms.maps.model.LatLng;

import java.io.Serializable;
import java.util.ArrayList;

public class Search extends AppCompatActivity {

    Firebase mPlantRef;
    Firebase mAttractionRef;

    EditText inputSearch;

    ArrayList<Plant> plantList;
    ArrayList<POI> poiList;
    ArrayList<Plant> plantSearchResults;
    ArrayList<POI> poiSearchResults;

    ListAdapter plantAdapter;
    PoiAdapter poiAdapter;

    ListView myPlantListView;
    ListView myPoiListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("  Search");

        toolbar.setLogo(R.mipmap.ic_launcher);
        toolbar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorGreen)));
        setSupportActionBar(toolbar);
        //==================== Firebase ===========================================================
        Firebase.setAndroidContext(Search.this);
        mPlantRef = new Firebase("https://botanicagarden.firebaseio.com/plants");
        mAttractionRef = new Firebase("https://botanicagarden.firebaseio.com/attractions");

        //==================== instantiate Controls ================================================
        inputSearch = (EditText) findViewById(R.id.et_searchPlant);
        inputSearch.clearFocus();

        plantList = new ArrayList<>();
        poiList = new ArrayList<>();

        //================== setup TABHOST==========================================================

        final TabHost tabHost = (TabHost) findViewById(R.id.tabHost);
        tabHost.setup();

        // Tab for Photos
        TabHost.TabSpec tabSpec = tabHost.newTabSpec("Plants");
        tabSpec.setContent(R.id.lv_searchPlant);
        tabSpec.setIndicator("Plants");
        tabHost.addTab(tabSpec);

        tabSpec = tabHost.newTabSpec("Attractions");
        tabSpec.setContent(R.id.lv_searchPois);
        tabSpec.setIndicator("Attractions");
        tabHost.addTab(tabSpec);

        setUpPlantsListView();

        tabHost.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
                if (tabId.equalsIgnoreCase("Attractions")) {
                    setUpAttractionsListView();

                } else if (tabId.equalsIgnoreCase("Plants")) {
                    setUpPlantsListView();
                }
            }
        });

        inputSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {

                String currentTag = tabHost.getCurrentTabTag();
                Log.d("CURRENT TAB", currentTag);


                Log.d("Textchange", "= " + cs + "  " + arg1 + " " + arg2 + " " + arg3);
                if (tabHost.getCurrentTabTag().equalsIgnoreCase("Attractions")) {
                    poiAdapter.getFilter().filter(cs);
                    poiSearchResults = new ArrayList<POI>();

                    //get the text in the EditText
                    String searchString = inputSearch.getText().toString();
                    int textLength = searchString.length();
                    poiSearchResults.clear();

                    for (int i = 0; i < poiList.size(); i++) {
                        String poiName = poiList.get(i).getName();
                        //System.out.println("player name "+playerName);
                        if (textLength <= poiName.length()) {
                           if (poiName.toLowerCase().contains(searchString.toLowerCase())) {
                                poiSearchResults.add(poiList.get(i));
                                Log.d("POI ssearch result: ",poiList.get(i).toString());
                                poiAdapter = new PoiAdapter(Search.this, R.layout.poi_row_item, poiSearchResults);
                                myPoiListView.setAdapter(poiAdapter);
                            }
                        }
                    }//end for loop

                    if (poiSearchResults.isEmpty()) {
                        Toast toast = Toast.makeText(getApplicationContext(),
                                "No Items Matched", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL, 0, 0);
                        toast.show();
                    }
                    poiAdapter.notifyDataSetChanged();


                }
                else if (tabHost.getCurrentTabTag().equalsIgnoreCase("Plants"))
                {
                    plantSearchResults = new ArrayList<Plant>();
                    Log.d("Textchange", "= " + cs + "  " + arg1 + " " + arg2 + " " + arg3);
                    plantAdapter.getFilter().filter(cs);

                    //get the text in the EditText
                    String searchString = inputSearch.getText().toString();
                    int textLength = searchString.length();
                    plantSearchResults.clear();

                    for (int i = 0; i < plantList.size(); i++) {
                        String plantName = plantList.get(i).getCommon_name();
                        //System.out.println("player name "+playerName);
                        if (textLength <= plantName.length()) {
                            //compare the String in EditText with Names in the ArrayList
                            //if(searchString.equalsIgnoreCase(plantName.substring(0,textLength)))
                            if (plantName.toLowerCase().contains(searchString.toLowerCase())) {
                                plantSearchResults.add(plantList.get(i));
                                // System.out.println("the array list is "+songsList.get(i));
                                plantAdapter = new ListAdapter(Search.this, R.layout.row_item, plantSearchResults);

                                myPlantListView.setAdapter(plantAdapter);
                            }
                        }
                    }
                    if (plantSearchResults.isEmpty()) {
                        Toast toast = Toast.makeText(getApplicationContext(),
                                "No Items Matched", Toast.LENGTH_SHORT);
                        toast.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL, 0, 0);
                        toast.show();
                    }
                    plantAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }


    public void setUpAttractionsListView() {





        poiList = new ArrayList<>();
        //PoiAdapter poiAdapter;
        // ListView myPoiListView;
        mAttractionRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot != null) {
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        POI poi = postSnapshot.getValue(POI.class);
                        // Log.d("Print DATA: ", poi.toString());
                        poiList.add(poi);
                    }

                    for (int i = 0; i < poiList.size(); i++) {
                        Log.d("MyPoi List :", poiList.get(i).toString());
                    }

                    myPoiListView = (ListView) findViewById(R.id.lv_searchPois);
                    poiAdapter = new PoiAdapter(Search.this, R.layout.poi_row_item, poiList);
                    myPoiListView.setAdapter(poiAdapter);

                    myPoiListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                            Intent i = new Intent(getApplicationContext(), PoiDetails.class);
                            i.putExtra("CURRENTPOI", (Serializable) poiList.get(position));
                            startActivity(i);
                        }
                    });

                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }// end of setUpAttractions

    public void setUpPlantsListView() {

        plantList = new ArrayList<>();
        //PoiAdapter poiAdapter;
        // ListView myPoiListView;
        mPlantRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot != null) {
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        Plant plant = postSnapshot.getValue(Plant.class);
                        // Log.d("Print DATA: ", poi.toString());
                        plantList.add(plant);
                    }

                    for (int i = 0; i < plantList.size(); i++) {
                        Log.d("MyPoi List :", plantList.get(i).toString());
                    }

                    myPlantListView = (ListView) findViewById(R.id.lv_searchPlant);
                    plantAdapter = new ListAdapter(getApplicationContext(), R.layout.row_item, plantList);
                    myPlantListView.setAdapter(plantAdapter);

                    myPlantListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Intent i = new Intent(Search.this, PlantDetail.class);
                            i.putExtra("CURRENTPLANT", (Serializable) plantList.get(position));
                            startActivity(i);
                        }
                    });
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }
}
