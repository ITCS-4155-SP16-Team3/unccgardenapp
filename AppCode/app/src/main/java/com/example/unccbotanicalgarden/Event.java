package com.example.unccbotanicalgarden;

import java.io.Serializable;

/**
 * Created by abc on 5/1/2016.
 */
public class Event implements Serializable {

    String cost, date, details, instructor, limit, location, name;

    public Event(){

    }

    public String getCost() {
        return cost;
    }

    public String getDate() {
        return date;
    }

    public String getDetails() {
        return details;
    }

    public String getInstructor() {
        return instructor;
    }

    public String getLimit() {
        return limit;
    }

    public String getLocation() {
        return location;
    }

    public String getName() {
        return name;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public void setInstructor(String instructor) {
        this.instructor = instructor;
    }

    public void setLimit(String limit) {
        this.limit = limit;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Event{" +
                "cost='" + cost + '\'' +
                ", date='" + date + '\'' +
                ", details='" + details + '\'' +
                ", instructor='" + instructor + '\'' +
                ", limit='" + limit + '\'' +
                ", location='" + location + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
