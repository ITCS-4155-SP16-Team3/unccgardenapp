package com.example.unccbotanicalgarden;

import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.squareup.picasso.Picasso;

public class PlantDetail extends AppCompatActivity {

    Plant currentPlant;
    TextView commonname, botanicalname,nativeto, size, light, soil, pests,facts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plant_detail);

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setIcon(R.mipmap.ic_launcher);
            actionBar.setTitle("  Plant Details");
            // actionBar.setLogo(R.mipmap.ic_launcher);
            actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorGreen)));
        }

        commonname =  ((TextView)findViewById(R.id.commonname));
        botanicalname =((TextView)findViewById(R.id.botanicalname));
       nativeto = ((TextView)findViewById(R.id.native_to));
       size = ((TextView)findViewById(R.id.size));
        light =  ((TextView)findViewById(R.id.light_req));
        soil = ((TextView)findViewById(R.id.soil_req));
        pests = ((TextView)findViewById(R.id.pests));
        facts = ((TextView)findViewById(R.id.facts));
        ImageView pimage =(ImageView)findViewById(R.id.plantImage);
        // bar.setDisplayShowTitleEnabled(false);
       // setSupportActionBar(actionBar);
        currentPlant = new Plant();

        if (getIntent() != null) {

         //   Log.d("getIntent", ": " + currentLatLng);
            currentPlant = (Plant)getIntent().getSerializableExtra("CURRENTPLANT");

            commonname.setText(currentPlant.getBotanical_name());
            botanicalname.setText(currentPlant.getCommon_name());
            nativeto.setText(currentPlant.native_to);
            size.setText(currentPlant.getSize());
            light.setText(currentPlant.getLight_req());
            soil .setText(currentPlant.getSoil_req());
            pests .setText(currentPlant.getPests());
            facts .setText(currentPlant.getFacts());

            Picasso.with(this)
                    .load(currentPlant.getImg())
                            //.fit()
                    .into(pimage);

        }
    }
}
