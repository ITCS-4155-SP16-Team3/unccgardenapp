package com.example.unccbotanicalgarden;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by abc on 5/5/2016.
 */
public class PoiAdapter extends ArrayAdapter<POI> {

    List<POI> myPois;
    Context myContext;
    int resource;
    //Plant currentPlant;


    public PoiAdapter(Context context, int resource, List<POI> objects) {
        super(context, resource, objects);
        this.resource = resource;
        this.myPois = objects;
        this.myContext = context;
        //this.currentPlant = null;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) myContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(resource, parent, false);
        }
        final POI currentPoi = myPois.get(position);

        TextView poiName = (TextView) convertView.findViewById(R.id.tv_poiName);
        TextView poidDistance = (TextView) convertView.findViewById(R.id.tv_poiDistance);
        ImageView poiImage = (ImageView) convertView.findViewById(R.id.iv_poiPics);
        ImageView plantLocation = (ImageView) convertView.findViewById(R.id.iv_poiLocation);

//        LocationManager service = (LocationManager) myContext.getSystemService(myContext.LOCATION_SERVICE);
//        Criteria criteria = new Criteria();
//        String provider = service.getBestProvider(criteria, false);
//        Location location = service.getLastKnownLocation(provider);
//        LatLng userLocation = new LatLng(location.getLatitude(),location.getLongitude());
//        Log.d("Location", userLocation.toString());
//
//        Location locationB = new Location("B");
//        locationB.setLatitude(Double.parseDouble(currentPoi.getLongitude()));//35.195541, -80.827037
//        locationB.setLongitude(Double.parseDouble(currentPoi.getLatittute()));
//        Log.d("LocationB", ": " + locationB);
//        Log.d("LocationB",": "+location);
//
//        double distance = location.distanceTo(locationB);
//        Log.d("LocationB",": "+ Math.round(distance* 0.00062137));
//        DecimalFormat df = new DecimalFormat("###.##");
//        Log.d("LocationB",":" + df.format(distance* 0.00062137));
//        String distancePoi = df.format(distance * 0.00062137)+"mi";
//        poidDistance.setText(distancePoi);

        poiName.setText(currentPoi.getName().replaceAll("_"," "));
        Picasso.with(getContext())
                .load(currentPoi.getImg())
                //.memoryPolicy(MemoryPolicy.NO_CACHE)
                                 //.fit()
                .into(poiImage);

        String locationUri = "@drawable/location";  // where myresource (without the extension) is the file
        int locationResource = convertView.getResources().getIdentifier(locationUri, null, myContext.getPackageName());
        Drawable resource1 = convertView.getResources().getDrawable(locationResource);
        plantLocation.setImageDrawable(resource1);
        plantLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), Navigator.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.putExtra("CURRENTPOI", (Serializable)currentPoi);
                myContext.startActivity(i);
            }
       });

        return convertView;
    }
}
