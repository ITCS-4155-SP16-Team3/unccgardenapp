package com.example.unccbotanicalgarden;

import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CalendarView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

//import com.firebase.client.Firebase;

public class Events extends AppCompatActivity {

    Firebase firebaseEventRef;
    ArrayList<Event> eventList;
    EventAdapter eventAdapter;
   // CalendarView myCal;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setIcon(R.mipmap.ic_launcher);
        actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorGreen)));
        actionBar.setTitle("  Events");

        eventList = new ArrayList<>();

//=========================== set Firebase references ==============================================
        Firebase.setAndroidContext(this);
        firebaseEventRef = new Firebase("https://botanicagarden.firebaseio.com/events/");

//==========================  fetech data from Firebase ===========================================

        firebaseEventRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot!=null)
                {
                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                       // Log.d("Print",postSnapshot.toString());
                        Event event = postSnapshot.getValue(Event.class);
                        Log.d("EVENT: ",event.toString());
                        eventList.add(event);
                        //notifyAll();
                    }



                    ListView eventListView = (ListView)findViewById(R.id.eventlistview);
                    eventAdapter = new EventAdapter(Events.this,R.layout.event_list,eventList);
                    eventListView.setAdapter(eventAdapter);
                }

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                Toast.makeText(getApplicationContext(),firebaseError.getMessage(),Toast.LENGTH_LONG).show();
            }
        });

    }
}
