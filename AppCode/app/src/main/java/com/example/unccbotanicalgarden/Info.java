package com.example.unccbotanicalgarden;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TabHost;

import com.squareup.picasso.Picasso;

public class Info extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setLogo(R.mipmap.ic_launcher);
        toolbar.setTitle("  Info");
        toolbar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorGreen)));
        setSupportActionBar(toolbar);

        ImageView mainImage = (ImageView)findViewById(R.id.infoMainImage);
        Picasso.with(getApplicationContext())
                .load("http://img2.10bestmedia.com/Images/Photos/109308/unc-charlotte-botanical-gardens-columbine_54_990x660_201405311944.jpg")
                .fit()
                .into(mainImage);

        TabHost tabHost = (TabHost) findViewById(R.id.tabHost);
        tabHost.setup();

        // Tab for Photos
        TabHost.TabSpec tabSpec = tabHost.newTabSpec("TAB1");
        tabSpec.setContent(R.id.services);
        tabSpec.setIndicator("Services");
        tabHost.addTab(tabSpec);

        tabSpec = tabHost.newTabSpec("TAB2");
        tabSpec.setContent(R.id.directions);
        tabSpec.setIndicator("Directions");
        tabHost.addTab(tabSpec);

        tabSpec = tabHost.newTabSpec("TAB3");
        tabSpec.setContent(R.id.etiquette);
        tabSpec.setIndicator("Etiquette");
        tabHost.addTab(tabSpec);

        tabSpec = tabHost.newTabSpec("TAB4");
        tabSpec.setContent(R.id.hours);
        tabSpec.setIndicator("Hours");
        tabHost.addTab(tabSpec);


    }

}
