package com.example.unccbotanicalgarden;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import java.util.ArrayList;

public class Gallery extends AppCompatActivity {

   // Firebase mFirebaseRef;
    ArrayList<Plant> plantList;
   ArrayList<String> imageList;

    ImageView myImage;
    GridView gridview;
    Firebase mFirebaseRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorGreen)));
        actionBar.setTitle("  Gallery");
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setIcon(R.mipmap.ic_launcher);

        Firebase.setAndroidContext(Gallery.this);
        mFirebaseRef = new Firebase("https://botanicagarden.firebaseio.com/plants");

        imageList =new ArrayList<>();
        plantList =new ArrayList<>();
        gridview = (GridView) findViewById(R.id.gridview);

        mFirebaseRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (dataSnapshot != null) {

                    for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        Plant plant = postSnapshot.getValue(Plant.class);
                        plantList.add(plant);
                    }
                }
                for (int i = 0; i < plantList.size(); i++) {
                    Log.d("Home :", plantList.get(i).getImg());
                    imageList.add(plantList.get(i).getImg());


                    gridview.setAdapter(new Grid_Adapter(Gallery.this, imageList));

                    gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        public void onItemClick(AdapterView<?> parent, View v,
                                                int position, long id) {
//                    Toast.makeText(Gallery.this, "" + position,
//                            Toast.LENGTH_SHORT).show();
                            Log.d("imagelist"," size = "+imageList.size());
                            Log.d("imagelist"," position = "+position);

                            Intent i = new Intent(Gallery.this, FullscreenImage.class);
                            i.putExtra("IMAGESLINKS", imageList);
                            i.putExtra("INDEX", position);
                            startActivity(i);


                        }
                    });

                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                Toast.makeText(getApplicationContext(), firebaseError.getMessage(), Toast.LENGTH_LONG);

            }
        });


    }
}