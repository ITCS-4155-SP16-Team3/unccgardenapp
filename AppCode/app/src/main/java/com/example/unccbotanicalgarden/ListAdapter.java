package com.example.unccbotanicalgarden;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by abc on 3/28/2016.
 */
public class ListAdapter extends ArrayAdapter<Plant> {

    List<Plant> myPlants;
    Context myContext;
    int resource;
    //Plant currentPlant;


    public ListAdapter(Context context, int resource, List<Plant> objects) {
        super(context, resource, objects);
        this.resource = resource;
        this.myPlants = objects;
        this.myContext = context;
        //this.currentPlant = null;
     }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) myContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(resource, parent, false);
        }
        final Plant currentPlant = myPlants.get(position);

        TextView bName = (TextView) convertView.findViewById(R.id.tv_botanicalname);
        TextView plantDistance = (TextView) convertView.findViewById(R.id.plantDistance);
        TextView cName = (TextView) convertView.findViewById(R.id.tv_commonname);
        ImageView plantImage = (ImageView) convertView.findViewById(R.id.iv_plantPics);
        ImageView plantLocation = (ImageView) convertView.findViewById(R.id.iv_plantLocation);

//        LocationManager service = (LocationManager) myContext.getSystemService(myContext.LOCATION_SERVICE);
//        Criteria criteria = new Criteria();
//        String provider = service.getBestProvider(criteria, false);
//        Location location = service.getLastKnownLocation(provider);
////        LatLng userLocation = new LatLng(location.getLatitude(),location.getLongitude());
//  //      Log.d("Location", userLocation.toString());
//
//        Location locationB = new Location("B");
//        locationB.setLatitude(Double.parseDouble(currentPlant.getLatittute()));//35.195541, -80.827037
//        locationB.setLongitude(Double.parseDouble(currentPlant.getLongitude()));
//        Log.d("LocationB", ": " + locationB);
//        Log.d("LocationB",": "+location);
//
//        double distance = location.distanceTo(locationB);
//        Log.d("LocationB",": "+ Math.round(distance* 0.00062137));
//        DecimalFormat df = new DecimalFormat("###.##");
//        Log.d("LocationB",":" + df.format(distance* 0.00062137));
//        String distancePlant = df.format(distance * 0.00062137)+"mi";
//        plantDistance.setText(distancePlant);
//









        String temp =currentPlant.getBotanical_name();
        String plantname = temp.replaceAll("_", " ");
        bName.setText(plantname);
        cName.setText(currentPlant.getCommon_name());

        //Log.d("IMG", );
        String url = currentPlant.getImg();
        Picasso.with(getContext())
                .load(url)
                 //.fit()
                .into(plantImage);


        String locationUri = "@drawable/location";  // where myresource (without the extension) is the file
        int locationResource = convertView.getResources().getIdentifier(locationUri, null, myContext.getPackageName());
        Drawable resource1 = convertView.getResources().getDrawable(locationResource);
        plantLocation.setImageDrawable(resource1);
        plantLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                  Intent i = new Intent(getContext(), Navigator.class);
                         i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.putExtra("CURRENTPLANT", (Serializable)currentPlant);
                myContext.startActivity(i);
            }
        });

        return convertView;
    }


}
