package com.example.unccbotanicalgarden;

import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class PoiDetails extends AppCompatActivity {

    POI currentPoi;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_poi_details);
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.colorGreen)));
        actionBar.setTitle("  Attraction Details");
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setIcon(R.mipmap.ic_launcher);
//actionBar.setTitle();


        currentPoi = null;
        ImageView poiDetails = (ImageView)findViewById(R.id.poiDetails);
        if(getIntent()!= null)
        {
            currentPoi = (POI)getIntent().getSerializableExtra("CURRENTPOI");
            Log.d("CURRENTPOI",currentPoi.getImg());
            Picasso.with(this)
                    .load(currentPoi.getImg())
                    .fit()
                    .into(poiDetails);
        }
    }
}
