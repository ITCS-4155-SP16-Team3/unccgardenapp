package com.example.unccbotanicalgarden;

import java.io.Serializable;

/**
 * Created by abc on 5/5/2016.
 */
public class POI implements Serializable {

    String name, img, latittute, longitude, trail;

    public POI() {

    }

    public POI(String img, String latittute, String longitude, String name, String trail) {
        this.img = img;
        this.latittute = latittute;
        this.longitude = longitude;
        this.name = name;
        this.trail = trail;
    }

    public String getImg() {
        return img;
    }

    public String getLatittute() {
        return latittute;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getName() {
        return name;
    }

    public String getTrail() {
        return trail;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public void setLatittute(String latittute) {
        this.latittute = latittute;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTrail(String trail) {
        this.trail = trail;
    }

    @Override
    public String toString() {
        return "POI{" +
                "img='" + img + '\'' +
                ", name='" + name + '\'' +
                ", latittute='" + latittute + '\'' +
                ", longitude='" + longitude + '\'' +
                ", trail='" + trail + '\'' +
                '}';
    }
}