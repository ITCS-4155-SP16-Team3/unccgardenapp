package com.example.unccbotanicalgarden;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.util.MonthDisplayHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by abc on 5/1/2016.
 */
public class EventAdapter extends ArrayAdapter<Event> {

    List<Event> myEvents;
    Context myContext;
    int resource;
    //Plant currentPlant;


    public EventAdapter(Context context, int resource, List<Event> objects) {
        super(context, resource, objects);
        this.resource = resource;
        this.myEvents = objects;
        this.myContext = context;
        //this.currentPlant = null;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) myContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(resource, parent, false);
        }
        final Event currentEvent = myEvents.get(position);

        TextView eventName = (TextView) convertView.findViewById(R.id.eventName);
        TextView eventLocation = (TextView) convertView.findViewById(R.id.eventLocation);
        TextView eventInstructor = (TextView) convertView.findViewById(R.id.eventInstructor);
        TextView eventCost = (TextView) convertView.findViewById(R.id.eventCost);
        TextView eventLimit = (TextView) convertView.findViewById(R.id.eventLimit);

        TextView eventMonth = (TextView) convertView.findViewById(R.id.month);
        TextView eventDate = (TextView) convertView.findViewById(R.id.date);

        String dateString = currentEvent.getDate().substring(0,(currentEvent.getDate().indexOf(',')));  //replaceAll(" ","");
        Log.d("Date",dateString);

            eventDate.setText(dateString.substring(0,3));//dateString.indexOf(' ')));
            eventMonth.setText(dateString.substring(dateString.indexOf(' ')+1, dateString.length()));
             Log.d("Date", eventDate.getText().toString());
             Log.d("Date", eventMonth.getText().toString());
        eventName.setText(currentEvent.getName());
        eventCost.setText(currentEvent.getCost());
        eventInstructor.setText(currentEvent.getInstructor());
        eventLimit.setText(currentEvent.getLimit());
        eventLocation.setText(currentEvent.getLocation());



        return convertView;
    }
}
