package com.example.unccbotanicalgarden;

import android.content.Context;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by abc on 4/24/2016.
 */
public class Grid_Adapter extends BaseAdapter {
    private Context mContext;
    private ArrayList<String> imageLink;

    public Grid_Adapter(Context c, ArrayList<String> imageLink) {
        this.mContext = c;
        this.imageLink = imageLink;
    }

    public int getCount() {
        return imageLink.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(int position, View convertView, ViewGroup parent) {

        Log.d("Gallery: ", "In Grid Adapter");
        ImageView imageView;
        if (convertView == null) {

            DisplayMetrics displayMetrics = mContext.getResources()
                    .getDisplayMetrics();

            int screenWidthInPix = displayMetrics.widthPixels;

            int screenheightInPix = displayMetrics.heightPixels;
            Log.d("SCREEN",screenWidthInPix+" X "+screenheightInPix );

            int layoutDim = (screenWidthInPix/3)-30 ;
            // if it's not recycled, initialize some attributes
            imageView = new ImageView(mContext);
            imageView.setLayoutParams(new GridView.LayoutParams(layoutDim, layoutDim));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(5,10,5,10);

        } else {
            imageView = (ImageView) convertView;
        }

        //imageView.setImageResource(mThumbIds[position]);
        Picasso.with(mContext)
                .load(imageLink.get(position))
                        //.fit()
                .into(imageView);



        return imageView;
    }

    // references to our images
//    private Integer[] mThumbIds = {
//            R.drawable.sample_2, R.drawable.sample_3,
//            R.drawable.sample_4, R.drawable.sample_5,
//            R.drawable.sample_6, R.drawable.sample_7,
//            R.drawable.sample_0, R.drawable.sample_1,
//            R.drawable.sample_2, R.drawable.sample_3,
//            R.drawable.sample_4, R.drawable.sample_5,
//            R.drawable.sample_6, R.drawable.sample_7,
//            R.drawable.sample_0, R.drawable.sample_1,
//            R.drawable.sample_2, R.drawable.sample_3,
//            R.drawable.sample_4, R.drawable.sample_5,
//            R.drawable.sample_6, R.drawable.sample_7
//    };
}