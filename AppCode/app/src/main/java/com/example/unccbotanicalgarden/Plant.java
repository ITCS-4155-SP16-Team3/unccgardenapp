package com.example.unccbotanicalgarden;

import java.io.Serializable;

/**
 * Created by abc on 3/28/2016.
 */
public class Plant implements Serializable {

    String botanical_name,common_name,facts,img,latittute,light_req,longitude,native_to,pests,size, soil_req;

   public Plant(){

   }

    public Plant(String botanical_name, String common_name, String facts, String img, String latittute, String light_req, String longitude, String native_to, String pests, String size, String soil_req) {
        this.botanical_name = botanical_name;
        this.common_name = common_name;
        this.facts = facts;
        this.img = img;
        this.latittute = latittute;
        this.light_req = light_req;
        this.longitude = longitude;
        this.native_to = native_to;
        this.pests = pests;
        this.size = size;
        this.soil_req = soil_req;
    }

    public void setBotanical_name(String botanical_name) {
        this.botanical_name = botanical_name;
    }

    public void setCommon_name(String common_name) {
        this.common_name = common_name;
    }

    public void setFacts(String facts) {
        this.facts = facts;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public void setLatittute(String latittute) {
        this.latittute = latittute;
    }

    public void setLight_req(String light_req) {
        this.light_req = light_req;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public void setNative_to(String native_to) {
        this.native_to = native_to;
    }

    public void setPests(String pests) {
        this.pests = pests;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public void setSoil_req(String soil_req) {
        this.soil_req = soil_req;
    }

    public String getBotanical_name() {

        return botanical_name;
    }

    public String getCommon_name() {
        return common_name;
    }

    public String getFacts() {
        return facts;
    }

    public String getImg() {
        return img;
    }

    public String getLatittute() {
        return latittute;
    }

    public String getLight_req() {
        return light_req;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getNative_to() {
        return native_to;
    }

    public String getPests() {
        return pests;
    }

    public String getSize() {
        return size;
    }

    public String getSoil_req() {
        return soil_req;
    }


    @Override
    public String toString() {
        return "Plant{" +
                "botanical_name='" + botanical_name + '\'' +
                ", common_name='" + common_name + '\'' +
                ", facts='" + facts + '\'' +
                ", img='" + img + '\'' +
                ", latittute='" + latittute + '\'' +
                ", light_req='" + light_req + '\'' +
                ", longitude='" + longitude + '\'' +
                ", native_to='" + native_to + '\'' +
                ", pests='" + pests + '\'' +
                ", size='" + size + '\'' +
                ", soil_req='" + soil_req + '\'' +
                '}';
    }
}

