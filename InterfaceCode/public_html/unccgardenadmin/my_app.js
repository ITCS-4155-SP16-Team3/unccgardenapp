function authWithPassword(userObj) {
    var deferred = $.Deferred();
    console.log(userObj);
    rootRef.authWithPassword(userObj, function onAuth(err, user) {
        if (err) {
            deferred.reject(err);
        }

        if (user) {
            deferred.resolve(user);
        }

    });

    return deferred.promise();
}


function getplants() {
    var ref = new Firebase('https://botanicagarden.firebaseio.com/plants/');
    //plantForm filleds
    var plantList = document.getElementById('listPlants');
    var childElementCount = plantList.childElementCount;

    var dataForm = document.getElementById("dataForm");
    // dataForm.style.visibility = 'hidden';
    if (childElementCount > 0) {
        for (var i = 0; i < childElementCount; i++)
        {
            plantList.removeChild(plantList.firstElementChild);
        }
    }

    ref.on("value", function(snapshot) {
        snapshot.forEach(function(data) {
            var li = document.createElement('li');
            var str = data.name();
            //alert(data.name());
            var name = str.split('_').join(' ');
            li.innerHTML = name;// + " ( " +data.val().trail+" )" ;
            li.style.border = '2px';
            li.addEventListener('click', function() {
                var str = this.innerHTML;
                //  alert("this.innerHTML = "+ this.innerHTML);
                var plant = str.split(' ').join('_');

                var refPlant = new Firebase('https://botanicagarden.firebaseio.com/plants/');
                refPlant.on("value", function(snapshot) {
                    //   alert("SnapShot",snapshot);
                    snapshot.forEach(function(data) {
                        //alert("data.name()",data.name());
                        if (data.name() === plant) {
                            // alert("Plant Name: " + data.name());
                            //alert("Plant Name: " + plant);
                            var childCount = dataForm.childElementCount;
                            //alert(childCount);
                            if (childCount > 0) {
                                for (var i = 0; i < childCount; i++)
                                {
                                    // alert(i);
                                    dataForm.removeChild(dataForm.firstElementChild);
                                }
                            }

                            //CREATE DOM ELEMENTS
                            var itemName = document.createElement('INPUT');
                            itemName.setAttribute("type", "text");
                            itemName.setAttribute("id", "nameItem");
                            itemName.setAttribute("type", "text");
                            itemName.setAttribute("placeholder", "Botanical Name");
                            itemName.disabled = true;
                            itemName.value = data.name();
                            //alert(data.name());
                            dataForm.appendChild(itemName);
                            dataForm.appendChild(document.createElement('br'));

                            var common_name = document.createElement('INPUT');
                            common_name.setAttribute("type", "text");
                            common_name.setAttribute("id", "common_name");
                            common_name.setAttribute("type", "text");
                            common_name.setAttribute("placeholder", "Common Name");
                            common_name.value = data.val().common_name;
                            dataForm.appendChild(common_name);
                            dataForm.appendChild(document.createElement('br'));

                            var lat = document.createElement('INPUT');
                            lat.setAttribute("type", "text");
                            lat.setAttribute("id", "latt");
                            lat.setAttribute("type", "text");
                            lat.setAttribute("placeholder", "Lattitude");
                            lat.value = data.val().latittute;
                            dataForm.appendChild(lat);
                            dataForm.appendChild(document.createElement('br'));

                            var long = document.createElement('INPUT');
                            long.setAttribute("type", "text");
                            long.setAttribute("id", "lng");
                            long.setAttribute("type", "text");
                            long.setAttribute("placeholder", "Longitude");
                            long.value = data.val().longitude;
                            dataForm.appendChild(long);
                            dataForm.appendChild(document.createElement('br'));

                            var imgLink = document.createElement('INPUT');
                            imgLink.setAttribute("type", "text");
                            imgLink.setAttribute("id", "imgurl");
                            imgLink.setAttribute("type", "text");
                            imgLink.setAttribute("placeholder", "Image Link");
                            imgLink.value = data.val().img;
                            dataForm.appendChild(imgLink);
                            dataForm.appendChild(document.createElement('br'));

//                            var botanical_name = document.createElement('INPUT');
//                            botanical_name.setAttribute("type", "text");
//                            botanical_name.setAttribute("id", "botanical_name");
//                            botanical_name.setAttribute("type", "text");
//                            botanical_name.setAttribute("placeholder", "Botanical Name");
//                            botanical_name.value = data.val().botanical_name;
//                            dataForm.appendChild(botanical_name);
//                            dataForm.appendChild(document.createElement('br'));

                            var native_to = document.createElement('INPUT');
                            native_to.setAttribute("type", "text");
                            native_to.setAttribute("id", "native_to");
                            native_to.setAttribute("type", "text");
                            native_to.setAttribute("placeholder", "Native To");
                            native_to.value = data.val().native_to;
                            dataForm.appendChild(native_to);
                            dataForm.appendChild(document.createElement('br'));

                            var light_req = document.createElement('INPUT');
                            light_req.setAttribute("type", "text");
                            light_req.setAttribute("id", "light_req");
                            light_req.setAttribute("type", "text");
                            light_req.setAttribute("placeholder", "Light Requirments");
                            light_req.value = data.val().light_req;
                            dataForm.appendChild(light_req);
                            dataForm.appendChild(document.createElement('br'));

                            var soil_req = document.createElement('INPUT');
                            soil_req.setAttribute("type", "text");
                            soil_req.setAttribute("id", "soil_req");
                            soil_req.setAttribute("type", "text");
                            soil_req.setAttribute("placeholder", "Soil Requirments");
                            soil_req.value = data.val().soil_req;
                            dataForm.appendChild(soil_req);
                            dataForm.appendChild(document.createElement('br'));

                            var size = document.createElement('INPUT');
                            size.setAttribute("type", "text");
                            size.setAttribute("id", "size");
                            size.setAttribute("type", "text");
                            size.setAttribute("placeholder", "Size");
                            size.value = data.val().size;
                            dataForm.appendChild(size);
                            dataForm.appendChild(document.createElement('br'));

                            var pests = document.createElement('INPUT');
                            pests.setAttribute("type", "text");
                            pests.setAttribute("id", "pests");
                            pests.setAttribute("type", "text");
                            pests.setAttribute("placeholder", "Pests");
                            pests.value = data.val().pests;
                            dataForm.appendChild(pests);
                            dataForm.appendChild(document.createElement('br'));

                            var facts = document.createElement('textarea');
                            facts.setAttribute("type", "text");
                            facts.setAttribute("id", "facts");
                            facts.setAttribute("type", "text");
                            facts.setAttribute("placeholder", "Intresting Facts");
                            facts.rows = 5;
                            facts.cols = 45;
                            facts.value = data.val().facts;
                            dataForm.appendChild(facts);
                            dataForm.appendChild(document.createElement('br'));

                            var savePlant = document.createElement('BUTTON');
                            savePlant.innerHTML = "SAVE";
                            savePlant.addEventListener('click', plantSaveHandler);
                            dataForm.appendChild(savePlant);
                            // dataForm.appendChild(document.createElement('br'));

                            var addPlant = document.createElement('BUTTON');
                            addPlant.innerHTML = "ADD";
                            addPlant.addEventListener('click', plantAddHandler);
                            dataForm.appendChild(addPlant);
                            //dataForm.appendChild(document.createElement('br'));

                            var deletePlant = document.createElement('BUTTON');
                            deletePlant.innerHTML = "DELETE";
                            deletePlant.addEventListener('click', plantDeleteHandler);
                            dataForm.appendChild(deletePlant);
                            // dataForm.appendChild(document.createElement('br'));

                            dataForm.style.visibility = 'visible';
//                                                var details = document.createElement('INPUT');
//                                                imgLink.setAttribute("type", "text");
//                                                imgLink.setAttribute("id", "imgurl");
//                                                imgLink.setAttribute("type", "text");

                        }
                    });
                });
            });
            plantList.appendChild(li);
        });
    });
}




function plantSaveHandler() {
    var ref = new Firebase('https://botanicagarden.firebaseio.com');
    //alert("IN HAndler - Plant");
    //plantForm filleds
    // var plantForm = document.getElementById("plantForm").value;
    var str = document.getElementById('nameItem').value;
    var nameItem = str.split(' ').join('_');
    var lng = document.getElementById('lng').value;
    var latt = document.getElementById('latt').value;
    var imgurl = document.getElementById('imgurl').value;
    //  var trails = document.getElementById('trails').value;

    // var botanical_name = document.getElementById('botanical_name').value;
    //var nameItem = str.split(' ').join('_');
    var common_name = document.getElementById('common_name').value;
    var native_to = document.getElementById('native_to').value;
    var light_req = document.getElementById('light_req').value;
    var soil_req = document.getElementById('soil_req').value;
    var size = document.getElementById('size').value;
    var pests = document.getElementById('pests').value;
    var facts = document.getElementById('facts').value;

    var usersRef = ref.child("plants");
    usersRef.child(nameItem).set({
        botanical_name: nameItem,
        common_name: common_name,
        facts: facts,
        img: imgurl,
        latittute: latt,
        light_req: light_req,
        longitude: lng,
        native_to: native_to,
        pests: pests,
        size: size,
        soil_req: soil_req
    });

    getplants();
    plantAddHandler();
}


function plantDeleteHandler() {

    var ref = new Firebase('https://botanicagarden.firebaseio.com/plants/');

    // //alert("In Delete");
    var str = document.getElementById('nameItem').value;
    var nameItem = str.split(' ').join('_');
    //alert(nameItem);

    ref.child(nameItem).set(null);
    plantAddHandler();

}

function plantAddHandler() {

    document.getElementById('nameItem').value = null;
    document.getElementById('nameItem').disabled = false;
    document.getElementById('lng').value = null;
    document.getElementById('latt').value = null;
    document.getElementById('imgurl').value = null;
    //document.getElementById('trails').value = null;
    //document.getElementById('botanical_name').value = null;
    document.getElementById('common_name').value = null;
    document.getElementById('native_to').value = null;
    document.getElementById('light_req').value = null;
    document.getElementById('soil_req').value = null;
    document.getElementById('size').value = null;
    document.getElementById('pests').value = null;
    document.getElementById('facts').value = null;
}

function getPOIs() {
    var ref = new Firebase('https://botanicagarden.firebaseio.com/attractions/');
    //plantForm filleds
    var plantList = document.getElementById('listPlants');
    var childElementCount = plantList.childElementCount;

    var dataForm = document.getElementById("dataForm");
    dataForm.style.visibility = 'hidden';
    if (childElementCount > 0) {
        for (var i = 0; i < childElementCount; i++)
        {
            plantList.removeChild(plantList.firstElementChild);
        }
    }

    ref.on("value", function(snapshot) {
        snapshot.forEach(function(data) {
            var li = document.createElement('li');
            var str = data.name();
            var name = str.split('_').join(' ');
            li.innerHTML = name;// + " ( " +data.val().trail+" )" ;
            li.addEventListener('click', function() {
                var str = this.innerHTML;
                var plant = str.split(' ').join('_');
                var refPlant = new Firebase('https://botanicagarden.firebaseio.com/attractions/');
                refPlant.on("value", function(snapshot) {
                    snapshot.forEach(function(data) {
                        if (data.name() === plant) {
                            // alert("Plant Name: " + data.name());
                            // alert("Plant Name: " + plant);

                            var dataForm = document.getElementById("dataForm");
                            var childCount = dataForm.childElementCount;
                            //alert(childCount);
                            if (childCount > 0) {
                                for (var i = 0; i < childCount; i++)
                                {
                                    // alert(i);
                                    dataForm.removeChild(dataForm.firstElementChild);
                                }
                            }


                            //CREATE DOM ELEMENTS

                            var itemName = document.createElement('INPUT');
                            itemName.setAttribute("type", "text");
                            itemName.setAttribute("id", "nameItem");
                            itemName.setAttribute("type", "text");
                            itemName.setAttribute("placeholder", "Name");
                            itemName.disabled = true;
                            itemName.value = data.name();
                            //alert(data.name());
                            dataForm.appendChild(itemName);
                            dataForm.appendChild(document.createElement('br'));

                            var long = document.createElement('INPUT');
                            long.setAttribute("type", "text");
                            long.setAttribute("id", "lng");
                            long.setAttribute("type", "text");
                            long.setAttribute("placeholder", "Longitude");
                            long.value = data.val().longitude;
                            dataForm.appendChild(long);
                            dataForm.appendChild(document.createElement('br'));

                            var lat = document.createElement('INPUT');
                            lat.setAttribute("type", "text");
                            lat.setAttribute("id", "latt");
                            lat.setAttribute("type", "text");
                            lat.setAttribute("placeholder", "Lattitude");
                            lat.value = data.val().latittute;
                            dataForm.appendChild(lat);
                            dataForm.appendChild(document.createElement('br'));

                            var imgLink = document.createElement('INPUT');
                            imgLink.setAttribute("type", "text");
                            imgLink.setAttribute("id", "imgurl");
                            imgLink.setAttribute("type", "text");
                            imgLink.setAttribute("placeholder", "Image Link");
                            imgLink.value = data.val().img;
                            dataForm.appendChild(imgLink);
                            dataForm.appendChild(document.createElement('br'));

                            var trail = document.createElement('INPUT');
                            trail.setAttribute("type", "text");
                            trail.setAttribute("id", "trails");
                            trail.setAttribute("type", "text");
                            trail.setAttribute("placeholder", "Trail");
                            trail.value = data.val().trail;
                            dataForm.appendChild(trail);
                            dataForm.appendChild(document.createElement('br'));
                            dataForm.appendChild(document.createElement('br'));

                            var savePOI = document.createElement('BUTTON');
                            savePOI.innerHTML = "SAVE";
                            savePOI.addEventListener('click', poiSaveHandler);
                            dataForm.appendChild(savePOI);
                            // dataForm.appendChild(document.createElement('br'));

                            var addPOI = document.createElement('BUTTON');
                            addPOI.innerHTML = "ADD";
                            addPOI.addEventListener('click', poiAddHandler);
                            dataForm.appendChild(addPOI);
                            //dataForm.appendChild(document.createElement('br'));

                            var deletePOI = document.createElement('BUTTON');
                            deletePOI.innerHTML = "DELETE";
                            deletePOI.addEventListener('click', poiDeleteHandler);
                            dataForm.appendChild(deletePOI);
                            // dataForm.appendChild(document.createElement('br'));

                            dataForm.style.visibility = 'visible';
//                                                var details = document.createElement('INPUT');
//                                                imgLink.setAttribute("type", "text");
//                                                imgLink.setAttribute("id", "imgurl");
//                                                imgLink.setAttribute("type", "text");

                        }
                    });
                });
            });
            plantList.appendChild(li);
        });
    });
}

function poiSaveHandler() {
    var ref = new Firebase('https://botanicagarden.firebaseio.com/');
    //alert("IN HAndler - Plant");
    //plantForm filleds
    // var plantForm = document.getElementById("plantForm").value;
    var str = document.getElementById('nameItem').value;
    var nameItem = str.split(' ').join('_');
    var lng = document.getElementById('lng').value;
    var latt = document.getElementById('latt').value;
    var imgurl = document.getElementById('imgurl').value;
    var trails = document.getElementById('trails').value;

    var usersRef = ref.child("attractions");

    usersRef.child(nameItem).set({
        img: imgurl,
        latittute: latt,
        longitude: lng,
        name: document.getElementById('nameItem').value,
        trail: trails
    });


    getPOIs();
}

function poiDeleteHandler() {

    var ref = new Firebase('https://botanicagarden.firebaseio.com/attractions/');

    //alert("In Delete");
    var str = document.getElementById('nameItem').value;
    var nameItem = str.split(' ').join('_');
    //alert(nameItem);

    ref.child(nameItem).set(null);

    document.getElementById('lng').value = null;
    document.getElementById('latt').value = null;
    document.getElementById('imgurl').value = null;
    document.getElementById('trails').value = null;
    getplants();
}

function poiAddHandler() {

    document.getElementById('nameItem').value = null;
    document.getElementById('nameItem').disabled = false;
    document.getElementById('lng').value = null;
    document.getElementById('latt').value = null;
    document.getElementById('imgurl').value = null;
    document.getElementById('trails').value = null;
}


//========================================== Events ============================

function getEvents() {
    var dataForm = document.getElementById("dataForm");
    dataForm.style.visibility = 'invisible';
    var ref = new Firebase('https://botanicagarden.firebaseio.com/events/');
    //plantForm filleds
    var plantList = document.getElementById('listPlants');
    var childElementCount = plantList.childElementCount;


    // dataForm.style.visibility = 'hidden';
    if (childElementCount > 0) {
        for (var i = 0; i < childElementCount; i++)
        {
            plantList.removeChild(plantList.firstElementChild);
        }
    }

    ref.on("value", function(snapshot) {
       // alert("Snapshot" + snapshot);
        snapshot.forEach(function(data) {
          //  alert("Data: " + data.name());

            var li = document.createElement('li');
            var name = data.val().name;//event name collected
          //  alert(data.val().name);
            li.innerHTML = name;
            li.style.border = '2px';
            li.addEventListener('click', function() {
                var event = this.innerHTML;
                var refEvent = new Firebase('https://botanicagarden.firebaseio.com/events/');
                refEvent.on("value", function(snapshot)
                {
                    snapshot.forEach(function(data)
                    {
                     //   alert("EVENT: " + data.val().name + " , " + data.val().date);
                        if (data.val().name === event)
                        {
                            var childCount = dataForm.childElementCount;
                            if (childCount > 0) {
                                for (var i = 0; i < childCount; i++) {
                                    dataForm.removeChild(dataForm.firstElementChild);
                                }
                            }

                            //CREATE DOM ELEMENTS
                            var eventName = document.createElement('INPUT');
                            eventName.setAttribute("type", "text");
                            eventName.setAttribute("id", "eventName");
                            eventName.setAttribute("placeholder", "Event Name");
                            eventName.disabled = true;
                            eventName.value = data.val().name;
                            //alert(data.name());
                            dataForm.appendChild(eventName);
                            dataForm.appendChild(document.createElement('br'));

                            var eventDate = document.createElement('INPUT');
                            eventDate.setAttribute("type", "text");
                            eventDate.setAttribute("id", "eventDate");
                            eventDate.setAttribute("placeholder", "Event Date");
                            eventDate.value = data.val().date;
                            dataForm.appendChild(eventDate);
                            dataForm.appendChild(document.createElement('br'));

                            var eventInst = document.createElement('INPUT');
                            eventInst.setAttribute("type", "text");
                            eventInst.setAttribute("id", "eventInst");
                            eventInst.setAttribute("placeholder", "Event Instructor");
                            eventInst.value = data.val().instructor;
                            dataForm.appendChild(eventInst);
                            dataForm.appendChild(document.createElement('br'));


                            var eventLoc = document.createElement('INPUT');
                            eventLoc.setAttribute("type", "text");
                            eventLoc.setAttribute("id", "eventLoc");
                            eventLoc.setAttribute("placeholder", "Event Location");
                            eventLoc.value = data.val().location;
                            dataForm.appendChild(eventLoc);
                            dataForm.appendChild(document.createElement('br'));

                            var eventCost = document.createElement('INPUT');
                            eventCost.setAttribute("type", "text");
                            eventCost.setAttribute("id", "eventCost");
                            eventCost.setAttribute("placeholder", "Event Cost");
                            eventCost.value = data.val().cost;
                            dataForm.appendChild(eventCost);
                            dataForm.appendChild(document.createElement('br'));

                            var eventLimit = document.createElement('INPUT');
                            eventLimit.setAttribute("type", "text");
                            eventLimit.setAttribute("id", "eventLimit");
                            eventLimit.setAttribute("placeholder", "Event Participant Limit");
                            eventLimit.value = data.val().limit;
                            dataForm.appendChild(eventLimit);
                            dataForm.appendChild(document.createElement('br'));

                            var eventDetails = document.createElement('INPUT');
                            eventDetails.setAttribute("type", "text");
                            eventDetails.setAttribute("id", "eventDetails");
                            eventDetails.setAttribute("placeholder", "Event Details");
                            eventDetails.value = data.val().details;
                            dataForm.appendChild(eventDetails);
                            dataForm.appendChild(document.createElement('br'));

                            var saveEvent = document.createElement('BUTTON');
                            saveEvent.innerHTML = "SAVE";
                            saveEvent.addEventListener('click', eventSaveHandler);
                            dataForm.appendChild(saveEvent);
//                            dataForm.appendChild(document.createElement('br'));

                            var addEvent = document.createElement('BUTTON');
                            addEvent.innerHTML = "ADD";
                            addEvent.addEventListener('click', eventAddHandler);
                            dataForm.appendChild(addEvent);
                            //   dataForm.appendChild(document.createElement('br'));

                            var deleteEvent = document.createElement('BUTTON');
                            deleteEvent.innerHTML = "DELETE";
                            deleteEvent.addEventListener('click', eventDeleteHandler);
                            dataForm.appendChild(deleteEvent);
                            dataForm.appendChild(document.createElement('br'));


                            dataForm.style.visibility = 'visible';
                        }
                    });
                });
            });//onclick
            plantList.appendChild(li);
        });//snapshot.foreach
    });//value
}//getEvents




function eventSaveHandler() {
   // alert("In Save Event");
    var ref = new Firebase('https://botanicagarden.firebaseio.com/events');
 // alert(ref);
    var str = document.getElementById('eventName').value;
    var nameItem = str.split(' ').join('_');
    ref.child(nameItem).set({
//        cost: "$45.00",
//        date: "05-01-2016",
//        details:"Summer gardening instructions",
//        instructor: "Mr. Gillman",
//        limit: "20",
//        location: "UNCC Greenhouse",
//        name: "Event1"
        
        cost: document.getElementById('eventCost').value,
        date: document.getElementById('eventDate').value,
        details: document.getElementById('eventDetails').value,
        instructor: document.getElementById('eventInst').value,
        limit: document.getElementById('eventLimit').value,
        location: document.getElementById('eventLoc').value,
        name: document.getElementById('eventName').value
    });

   getEvents();
   eventAddHandler();
}


function eventDeleteHandler() {

    var ref = new Firebase('https://botanicagarden.firebaseio.com/events/');
    var childname = "";
    ref.on("value", function(snapshot) {
        snapshot.forEach(function(data) {
            if (data.val().name === document.getElementById('eventName').value) {
                childname = data.name();
            }
        });

    });
    //alert(childname);

//
//    // //alert("In Delete");
//    var eventItem = document.getElementById('nameItem').value;
//    ref.child(nameItem).set(null);
//    plantAddHandler();

}

function eventAddHandler() {

    document.getElementById('eventName').value = null;
    document.getElementById('eventName').disabled = false;
    document.getElementById('eventCost').value = null;
    document.getElementById('eventDate').value = null;
    document.getElementById('eventDetails').value = null;
    document.getElementById('eventInst').value = null;
    document.getElementById('eventLimit').value = null;
    document.getElementById('eventLoc').value = null;

}
