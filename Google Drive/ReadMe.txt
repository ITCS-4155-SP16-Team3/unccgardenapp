ITCS 4155 Team 3 "Who Did What in Google Drive" Notes for BitBucket

Format:
Folder Name - Folder Creator
	* File Name - File Contributors

----- Begin 


4155 Project - Julian

	Geolocation/Android Info - Stephanie
		Coordinates - Stephanie
			* Coordinates Images - Stephanie
		Coordinates2 - Elizabeth
			* Coordinates2 Images - Elizabeth
		Path Coordinates - Elizabeth
			* Note_ Not Capturing Points Correctly! Doc - Elizabeth
			* PathCoordsFiles.txt - Elizabeth
			* PathCoordFiles.gpx - Elizabeth
		* Reference - Stephanie, Elizabeth

	Lo-Fi Demo - Stephanie
		* wireframe.pdf - Susama
		* wireframe - Susama, Elizabeth (can't check file history for other contributors)
		* new doc 3.pdf - Julian

	Sprint 1 Documents - Elizabeth
		Route 1 Plants - Elizabeth
			* PlantPics.jpg - Elizabeth
		* Design Document - Elizabeth
		* Letter Grade Contract - Elizabeth
		* Plant Names - Elizabeth (names), Stephanie and Susama (coordinates)

	Stakeholder Meetings/Tips - Juan
		*1st Stakeholder Meeting.docx - Juan

	* 4155 Project Proposal - Julian, Juan, Stephanie, Susama, Elizabeth

